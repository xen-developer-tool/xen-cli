import os
import sys
import click
from xenlib.helper.config.environment import Environment
from xenlib.helper.api.project import check_project_execution

pass_environment = click.make_pass_decorator(Environment, ensure=True)

@click.command('run')
@pass_environment
def action(env):
    """checkin help"""
    click.echo('Running: {}'.format(env.project_name))
    execution = check_project_execution()
    print("Project Execution detail : ")
    print("  Project name: {}".format(execution['projects_name']))
    print("  Dev Public IP: {}".format(execution['execution_ip']))
    print("  Dev App Session Port: {}".format(execution['execution_port']))
    print("  Domain alias: {}".format(execution['execution_domain']))
    try:
        LOCAL_PORT=81
        LOCAL_HOST='localhost'
        print(f"Tunneling DEV Server App Session to http://{LOCAL_HOST}:{LOCAL_PORT} ...")
        #os.system("python ..\\xen-portforward\port-forward.py {}:{}:{}".format('81',execution['execution_ip'],execution['execution_port']))
        os.system(f'set SERVER_PORT={LOCAL_PORT}&php artisan serve --host={LOCAL_HOST}')
    except KeyboardInterrupt:
        print('\nExecution aborted..')
        sys.exit()

    
