import os
import sys
from datetime import datetime

def command_log(func):

    def xen_log(*args, **kwargs):
        xen_history = os.path.join(os.path.expanduser("~"),".xen-history")
        xen_log = os.path.join(os.path.expanduser("~"),"xen.log")
        head, tail = os.path.split(sys.argv[0:][0])
        xen_args = list(sys.argv[1:])
        command = tail+" "+" ".join(xen_args)
        with open(xen_history, 'a+') as f: f.write(command+"\n")
        log = "{} - {}".format(datetime.now(), str(" ".join(sys.argv[0:])))
        with open(xen_log, 'a+') as f: f.write(log+"\n")
        func(*args, **kwargs)
    
    return xen_log