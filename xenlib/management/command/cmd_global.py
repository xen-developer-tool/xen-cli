import os
import click
import git
from typing_extensions import Required
from xenlib.cli import pass_environment
#from xenlib.management.action.global import register_actions

def find_kown_gitrepo(home_path):
    git_servers = ['gitlab', 'github', 'bitbucket','codecommit', 'codebase', 'beanstalk']
    known_hosts = open(os.path.join(home_path, ".ssh","known_hosts"), "r")
    found = {}
    for line in known_hosts.readlines():
        for g in git_servers:
            if g in line:
                found[g] = True
    return list(found.keys())

@click.command()
def cli():
    """Global config management"""
    home_path = os.path.expanduser("~")
    home_gitconfig = os.path.join(home_path,".gitconfig")
    home_ssh_pubkey = os.path.join(home_path,".ssh","id_rsa.pub")
    rsapubkey = open(home_ssh_pubkey, "r")
    globalconfig = git.GitConfigParser([os.path.normpath(os.path.expanduser("~/.gitconfig"))], read_only=True)
    
    print(f'User home dir: {home_path}')
    print(f'Global gitconfig: {home_gitconfig}')
    print(f'SSH public key: {home_ssh_pubkey}')
    print(rsapubkey.read()) 
    print("Git user name:", globalconfig.get_value('user','name'))
    print("Git user email:", globalconfig.get_value('user','email'))

    kown_gitrepo = find_kown_gitrepo(home_path)
    print(f"Known git repo: {kown_gitrepo}")
    #for key in found: print(key, end=" ")
    pass

#register_actions(cli)