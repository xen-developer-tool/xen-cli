import os
import sys
import click
from xenlib.helper.config.environment import Environment
from xenlib.helper.api.project import check_project_execution

pass_environment = click.make_pass_decorator(Environment, ensure=True)

@click.command('status')
@pass_environment
def action(env):
    """checkin help"""
    click.echo('Running: {}'.format(env.project_name))
    

    
